var d3 = require('d3');

module.exports = {
    chartType: {
        'BAR': 'bar',
        'PIE': 'pie',
        'LINE': 'line'
    },
    events: {
        'ERROR': 'error',
        'DATA_PROCESSING': 'dataProcessing',
        'DATA_PROCESSING_END': 'dataProcessingEnd',
        'CHART_RENDERING': 'chartRendering',
        'CHART_RENDERING_END': 'chartRenderingEnd',
        'FETCHING_DATA': 'fetchingData',
        'FETCHING_DATA_END': 'fetchingDataEnd'
    },
    aggregation: {
        'AVG': function(avg, n1, n){
            return (n * avg + n1)/(n + 1);
        },
        'COUNT': function(n){
            return n + 1;
        },
        'SUM': function(sum, n1){
            return sum + n1;
        }
    },
    tickFormats: {
        'numeric': d3.format('>0,.2s'),
        'currency': d3.format('>$0,.2s'),
        'percent': d3.format('0%'),
        'date': d3.time.format('%m/%d/%y')
    },
    dataType: {
        'TEXT': 'text',
        'NUMERIC': 'numeric',
        'DATE': 'date'
    },
    dataSubtype: {
        'CURRENCY': 'currency',
        'PCT': 'percent'
    }
};