var logger = require('./logger.js'),
    log = logger.create("axes.js"),
    d3 = require('d3'),
    cc = require('./ChartConst.js');

function calculateValues(chart, series, d, o) {
    var //isXAxis = (o.xAxis === series),
        aggregation = series.aggregation || cc.aggregation.AVG,
        col = series.colName,
        calculatedVals = {},
        xAxis = o.xAxis;

    if (!col) {
        if (aggregation !== cc.aggregation.COUNT) {
            log.error("Series must refer to a column or should have the aggregation function of COUNT");
            chart.triggerEvt(cc.events.ERROR, "Series must refer to a column or should have the aggregation function of COUNT");
            return null;
        }

        col = xAxis.colName;
        series.dataType = cc.dataType.NUMERIC;
    }
    
    var values = d.map(function (c) {
        return c[col];
    });


    //if(isXAxis) return d3.set(values.sort()).values();

    d.forEach(function (c) {
        var xVal = c[xAxis.colName],
            yVal = c[col],
            calculatedVal = calculatedVals[xVal] || {
                val: 0,
                num: 0
            };

        calculatedVal.val = ((col===xAxis.colName) ? yVal : aggregation(calculatedVal.val, yVal, calculatedVal.num));
        calculatedVal.num++;

        calculatedVals[xVal] = calculatedVal;
    });

    return calculatedVals;
}

function calculateAxes(chart, serieses) {
    var axesValsByType = {},
        axesByType = {},
        noOfTypes = 0;

    serieses.forEach(function (series) {
        if (!series.dataType) {
            log.error("Unexpected Series data type. Returning...");
            chart.triggerEvt(cc.events.ERROR, "Unexpected Series data type. Returning...");
            return;
        }

        var seriesVals = d3.values(series.calcValues).map(function (c) {
                return (series.aggregation===cc.aggregation.COUNT) ? c.num : c.val;
            }),
            axesData, type;

        switch (series.dataType) {
        case cc.dataType.NUMERIC:
            type = series.dataSubtype || cc.dataType.NUMERIC;
            break;
        default:
            type = series.dataType;
        }

        if (!axesValsByType[type]) {
            if (noOfTypes === 2) {
                log.warn("Limit of axes types reached. Cannot render series for column, " + series.colName + ", of type, " + type + ".");
                return;
            }

            axesData = seriesVals.sort();
            axesValsByType[type] = axesData;

            noOfTypes++;
        } else {
            axesData = axesValsByType[type].concat(seriesVals).sort();
            axesValsByType[type] = axesData;
        }
    });

    d3.keys(axesValsByType).forEach(function (key) {
        var values = axesValsByType[key],
            scale = getScale(key, values);

        axesByType[key] = d3.svg.axis().scale(scale);
    });

    return axesByType;
}

function getScale(dataType, values){
    var scale;
    switch (dataType) {
        case cc.dataType.TEXT:
            scale = d3.scale.ordinal().domain(values);
            break;
        case cc.dataType.DATE:
            scale = d3.time.scale().domain([d3.min(values), d3.max(values)]);
            break;
        case cc.dataSubtype.PCT:
            scale = d3.scale.linear().domain([Math.min(0, d3.min(values))/100, Math.max(100, d3.max(values))/100]);
            break;
        default:
            scale = d3.scale.linear().domain([Math.min(0, d3.min(values)), d3.max(values)*1.5]);
    }
    
    return scale;
}

function calculateXAxis(chart, series) {
    var seriesVals = d3.values(series.calcValues).map(function (c) {
                return c.val;
            }),
        axesData = seriesVals.sort(),
        dataType = series.dataSubtype ? series.dataSubtype: series.dataType,
        axes = {};
        
    [cc.chartType.BAR, cc.chartType.LINE].forEach(function(type, idx){
        var scale;
        
        switch(type){
            case cc.chartType.BAR:
                scale = d3.scale.ordinal().domain(axesData);
                break;
            case cc.chartType.LINE:
                scale = getScale(dataType, axesData);
        }
        
        axes[type] = d3.svg.axis().scale(scale);
    });
    
    return axes;
}

function processData(chart, series, d, o) {
    if (!series) {
        log.info("Invalid series or series is null. Returning empty object...");
        return {};
    }

    var calcValues = calculateValues(chart, series, d, o);

    return $.extend({}, series, {
        "calcValues": calcValues
    });
}

function renderYAxes(chart, cont) {
    var series = chart.metaData.series,
        yAxes = chart.metaData.yAxisObjs,
        dims = chart.dimensions.plotArea,
        ticksAtEqualPos = chart.opts.ticksAtEqualPos,
        tickInterval = 0;

    chart.yAxesElems = {};

    if (ticksAtEqualPos) {
        tickInterval = dims.height / 10;
    }

    var axisElems = cont.selectAll('g.axis.y-axis').data(d3.keys(yAxes));
    
    axisElems.enter().append('g').attr('class','axis y-axis');

    axisElems.each(function (key, idx) {
        var g = d3.select(this),
            axisObj = yAxes[key],
            orient = (idx === 0) ? 'left' : 'right';

        g.classed(orient, true).classed(key, true);

        axisObj.orient(orient)
            .ticks(10)
            .scale()
            .range([dims.height, 0]);

        if (tickInterval > 0) {
            var ticksValues = [];
            for (var i = 0; i < dims.height; i += tickInterval) {
                ticksValues.push(axisObj.scale().invert(i));
            }

            axisObj.tickValues(ticksValues);
        } else {
            axisObj.tickValues(null);
        }

        if (key !== cc.dataType.TEXT) {
            axisObj.tickFormat(cc.tickFormats[key])
        }

        g.transition().duration(300).call(axisObj);

        var width = g.node().getBBox().width,
            sign = (idx === 0) ? '-' : '';

        log.info(width);

        g.selectAll('text.title')
            .data([series.reduce(function (str, s) {
                var type = s.dataSubtype || s.dataType,
                    title = (s.title || s.colName || "");

                if (type === key && title !== "") {
                    return str + ((str != "") ? ", " : "") + title;
                }

                return str;
            }, "")])
            .enter()
            .append('text')
            .transition().delay(300)
            .each(function(title){
                var textElem = d3.select(this);
                
                textElem
                    .text(title)
                    .attr({
                        'transform': 'rotate(' + sign + '90) translate(' + sign +
                            dims.height / 2 + ',-' + (width + ((idx === 0) ? 0 : 5)) + ')',
                        'class': 'title'
                    });
                
                var dx = ((idx === 0) ? g.node().getBBox().width : dims.x + dims.width - g.node().getBBox().width);

                if (idx === 0) {
                    dims.x = dx;
                    dims.width = dims.width - dims.x;
                } else {
                    dims.width = dx - dims.x;
                }

                g.transition().duration(400).attr('transform', 'translate(' + dx + ',0)');
            });
        
        chart.yAxesElems[key] = g;
    });

    cont.selectAll('.axis.left .tick line').transition().delay(300).attr('x2', dims.width);
    cont.selectAll('.axis.right .tick line').transition().delay(300).attr('x2', -dims.width);
}

function renderXAxis(chart, cont, type) {
    var dims = chart.dimensions.plotArea,
        chartType = type || d3.keys(chart.metaData.xAxisObjs)[0],
        xAxisObj = chart.metaData.xAxisObjs[chartType],
        axisElems = cont.selectAll('g.axis.x-axis').data([xAxisObj]);
    
    axisElems.enter().append('g').attr('class','axis x-axis');
    
    axisElems.each(function(axisObj) {
        var g = d3.select(this);
        
        axisObj
            .orient('bottom')
            .scale().range([0, dims.width]);
        
        if(chartType === cc.chartType.BAR) {
            axisObj.scale().rangeBands([0, dims.width], 0.25, 0.5);
        }else if(chart.metaData.xAxis.dataType === cc.dataType.TEXT){
            axisObj.scale().rangePoints([0, dims.width], 0.5);
        }
        
        g.call(axisObj)
            .attr('transform', 'translate('+dims.x+','+chart.dimensions.height+')');
            
        g.transition()
            .delay(300)
            .duration(500)
            .attr('transform', 'translate('+dims.x+','+dims.height+')');
    });
}

function render(chart) {
    var chartArea = chart.svg.select('.chart-area');

    renderYAxes(chart, chartArea);
    renderXAxis(chart, chartArea);
}

module.exports = {
    processData: processData,
    processAxes: calculateAxes,
    processXAxis: calculateXAxis,
    render: render
};