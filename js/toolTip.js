var $ = require('jquery'),
    cc = require('./ChartConst.js');

function show(value, s, $cont, coords){
    var $toolTip = $cont.find('.tool-tip');
    
    if(!$toolTip.length){
        $toolTip = $('<div/>').appendTo($cont).addClass('tool-tip');
    }
    
    $toolTip.html('<div class="y-val"\
                    <label>'+s.colName+'</label>\
                    <span class="val">'+value+((cc.dataSubtype.PCT===s.dataSubtype)?'%':'')+'</span>\
                </div>')
        .css({
            'left': coords[0]+5,
            'top': coords[1]+5
        })
        .show();   
}

function hide($cont){
    $cont.find('.tool-tip').hide();
}

module.exports = {
    show: show,
    hide: hide
};