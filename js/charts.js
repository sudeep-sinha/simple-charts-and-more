var cc = require('./ChartConst.js'),
    bar = require('./bar.js'),
    line = require('./line.js'),
    pie = require('./pie.js'),
    logger = require('./logger.js'),
    axes = require('./axes.js'),
    transitions = require('./transitions.js'),
    $ = require('jquery'),
    d3 = require('d3'),
    log = logger.create("charts.js"),
    factory = {},
    defaultOpts = {
        width: 800,
        height: 500,
        onError: $.noop,
        onDataProcessing: $.noop,
        onDataProcessingEnd: $.noop,
        onChartRendering: $.noop,
        onChartRenderingEnd: $.noop,
        onFetchingData: $.noop,
        onFetchingDataEnd: $.noop,
        type: cc.chartType.BAR,
        ticksAtEqualPos: true,
        xAxis: {},
        series: []
    },
    UNDEF = 'undefined';

/*********************************************************************************************************************

*********************************************************************************************************************/

factory[cc.chartType.BAR] = bar;
factory[cc.chartType.LINE] = line;
factory[cc.chartType.PIE] = pie;

var ScamChart = function ScamChart(domElem, opts, data){
    var self = this,
        $me = $(domElem),
        o = $.extend({}, defaultOpts, opts),
        myType = o.type;
    
    self.data = data;
    
    function fetchData(){
        var $def = $.Deferred();
        
        if(!self.data && !opts.dataUrl){
            self.triggerEvt(cc.events.ERROR, "Please provide data or provide a url from where data can be fetched");
            log.error("Please provide data or provide a url from where data can be fetched");
            
            $def.reject();
        } else {
            if(self.data){
                $def.resolve(self.data);
            }else{
                log.debug("Fetching Data.....");
                var xhr = $.ajax(opts.dataUrl, {
                    success: function(xhr, d){
                        log.debug("Data fetch successful....");
                        self.triggerEvt(cc.events.FETCHING_DATA_END);
                        
                        //The library expects a comma separated string to be returned from the server
                        $def.resolve(d3.csv.parse(d));
                    },
                    error: function(){
                        log.error("Error fetching data");
                        
                        $def.reject();
                    }
                });
                
                self.triggerEvt(cc.events.FETCHING_DATA, xhr);
            }
        }
        
        return $def.promise();
    }

    function processData(){
        return fetchData().pipe(function(d){
            self.triggerEvt(cc.events.DATA_PROCESSING);
            
            var metaData = {};
            
            //configure x-axis
            metaData.xAxis = axes.processData(self, opts.xAxis, d, o);
            metaData.series = [];
            opts.series && opts.series.forEach(function(axis){
                metaData.series.push(axes.processData(self, axis, d, o));
            });
            
            metaData.yAxisObjs = axes.processAxes(self, metaData.series);
            metaData.xAxisObjs = axes.processXAxis(self, metaData.xAxis);
            
            self.data = d;
            self.metaData = metaData;
            
            self.triggerEvt(cc.events.DATA_PROCESSING_END);
            return metaData;
        });
    }
    
    function renderChart(d){
        //TODO: render chart here
        self.triggerEvt(cc.events.CHART_RENDERING);
        
        var dims = self.dimensions;
        
        self.svg
            .attr({width: dims.width, height: dims.height})
            .selectAll('g.chart-area')
            .data([1])
            .enter()
                .append('g')
                .attr({
                    'class': 'chart-area',
                    'height': dims.chartArea.height,
                    'width': dims.chartArea.width,
                    'transform': 'translate('+dims.chartArea.x+','+dims.chartArea.y+')'
                });
        
        
        axes.render(self);
        
        switch(o.type){
            case cc.chartType.BAR : 
                bar.render(self);
                break;
        }
        
        //render the chart outline first
        self.$me.addClass('chart');
        
        self.triggerEvt(cc.events.CHART_RENDERING_END);
    }
    
    function switchTo(chartType){
        transitions.switchTo(self.metaData.currType || myType, chartType, self);
    }
    
    (function init(){
        if(!$me.length){
            self.triggerEvt(cc.events.ERROR, "Please provide a valid DOM element to render the chart on");
            log.error("Please provide a valid DOM element to render the chart on");
            return;
        }
        
        self.$me = $me;
        self.opts = o;
        self.svg = d3.select($me[0]).append('svg');
        self.reProcessAndRender = function(){
            processData().done(renderChart);
        };
        self.render = renderChart;
        self.switchTo = switchTo;
        
        self.dimensions = {
            width : self.opts.width,
            height : self.opts.height,
            chartArea : {
                width: self.opts.width - 10,
                height: self.opts.height - 100,
                x: 5,
                y: 15
            },
            plotArea : {
                width: self.opts.width - 10,
                height: self.opts.height - 100,
                x: 0,
                y: 0
            }
        };
        
        self.reProcessAndRender();
    })();
};

ScamChart.prototype.triggerEvt = function triggerEvt(eventName, msg){
    var fullEventName = "on"+eventName.replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
    
    log.debug("Triggering event "+fullEventName);
    
    if(this.opts[fullEventName]){
        this.opts[fullEventName].call(this, msg);
    }
};

ScamChart.prototype.option = function option(name, value){
    if(name){
        if(typeof value !== UNDEF){
            this.opts[name] = value;
            return this;
        }else{
            return this.opts[name];
        }
    }
    
    return null;
};

var exprts = {
    create: function (domElem, opts, data) {
        return new ScamChart(domElem, opts, data);
    }
};

module.exports = $.extend({},cc,exprts);