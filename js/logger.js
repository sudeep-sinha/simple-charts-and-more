var Logger = function Logger(clazz){
    if(!clazz)
        throw new Error("Invalid class or clazz is null");
    
    function log(type, msg){
        try {
            console[type](msg);
        }catch(e){}
    }

    return {
        error: function(msg){
            log("error", msg + " at ["+clazz+"]");
        },
        info: function(msg){
            log("info", msg);
        },
        warn: function(msg){
            log("warn", msg);
        },
        debug: function(msg){
            log("debug", msg);
        }
    };
}

module.exports = {
    create: function(clazz){
        return new Logger(clazz);
    }
};