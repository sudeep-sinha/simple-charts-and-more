var cc = require('./ChartConst.js'),
    transitions = require('./transitions.js'),
    d3 = require('d3'),
    logger = require('./logger.js'),
    toolTip = require('./toolTip.js'),
    utils = require('./utils.js'),
    log = logger.create("bar.js");

function render(chart){
    //var self = this;
    //this.type = cc.chartType.BAR;
    var meta = chart.metaData,
        data = chart.data,
        opts = chart.opts,
        yAxisObjs = meta.yAxisObjs,
        xScale = meta.xAxisObjs[cc.chartType.BAR].scale(),
        bandWdth = xScale.rangeBand(),
        plotableSeries = meta.series.reduce(function(p, s){
            var type = s.dataSubtype || s.dataType,
                yAxisObj = yAxisObjs[type];
            
            if(yAxisObj)
                p.push(s);
            
            return p;
        }, []),
        colors = d3.scale.category20(),
        barWdth = bandWdth / plotableSeries.length,
        plotArea = chart.svg.select('.chart-area')
                            .selectAll('g.plot-area')
                            .data([1])
                            .enter()
                            .append('g')
                            .attr({
                                'class': 'plot-area',
                                'transform': 'translate('+chart.dimensions.plotArea.x+','+chart.dimensions.plotArea.y+')'
                            });
    
    meta.plotableSeries = plotableSeries;
    
    //Wrap x axis labels if required
    chart.svg.select('.chart-area')
            .selectAll('.x-axis .tick text')
            .call(utils.wrap, bandWdth);
            
    
    plotArea.selectAll('g.series')
        .data(plotableSeries)
        .enter()
        .append('g')
        .each(function(s, i){
            var series = d3.select(this),
                type = s.dataSubtype || s.dataType,
                yAxisObj = yAxisObjs[type];
            
            series
                .attr('class','series series-'+i)
                .selectAll('path.point')
                .data(d3.entries(s.calcValues))
                .enter()
                .append('path')
                .on('mouseenter',function(d){
                    plotArea.selectAll('.series')
                            .filter(function(){
                                return this !== series.node();
                            })
                            .classed('blur',true);
                    chart.svg.select('.chart-area .y-axis.'+type)
                        .style({
                            'stroke': 'RED'
                        });
                    var coords = d3.mouse(chart.$me[0]);
                    if(coords){
                        toolTip.show(d.value.val, s, chart.$me, coords);
                    }
                })
                .on('mouseleave',function(d){
                    plotArea.selectAll('.series')
                            .filter(function(){
                                return this !== series.node();
                            })
                            .classed('blur',false);
                    
                    chart.svg.select('.chart-area .y-axis.'+type)
                        .style({
                            'stroke': 'none'
                        });
                    toolTip.hide(chart.$me);
                })
                .each(function(d, j){
                    var xVal = d.key,
                        yVal = d.value,
                        yScale = yAxisObj.scale(),
                        y = yScale((type!==cc.dataSubtype.PCT) ? yVal.val : yVal.val/100),
                        x = xScale(xVal) + barWdth * i,
                        path = d3.select(this);
                    
                    if(s.aggregation === cc.aggregation.COUNT)
                        y = yScale(yVal.num);
                    
                    var c = i*2;
                    log.info(colors(c)+', '+c+', '+ i+', '+(i*2));
                    
                    path.attr({
                        'transform': 'translate('+x+','+chart.dimensions.plotArea.height+')',
                        'd': transitions.getBarPath(barWdth-2, 0)
                    })
                    .transition()
                    .delay(700)
                    .style({
                        'fill':colors(i*2),
                    })
                    .duration(500)
                    .attr({
                        'class': 'point',
                        'transform': 'translate('+x+','+y+')',
                        'd': transitions.getBarPath(barWdth-2, (chart.dimensions.plotArea.height-y))
                    });
                });
        });
}
    
module.exports = {
    render: function(chart) {
        render(chart);
    }
};