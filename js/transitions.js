var d3 = require('d3'),
    cc = require('./ChartConst.js'),
    utils = require('./utils.js');

var switchers = {};

switchers[cc.chartType.BAR] = {};
switchers[cc.chartType.BAR][cc.chartType.LINE] = barToLine;

switchers[cc.chartType.LINE] = {};
switchers[cc.chartType.LINE][cc.chartType.BAR] = lineToBar;

function barToLine(chart){
    var meta = chart.metaData,
        yAxisObjs = meta.yAxisObjs,
        xScale = meta.xAxisObjs[chart.opts.type].scale(),
        plotableSeries = meta.plotableSeries,
        bandWdth = 0 || xScale.rangeBand(),
        barWdth = bandWdth/plotableSeries.length,
        colors = d3.scale.category20();
    
    var plotArea = chart.svg.select('.plot-area');
    
    plotArea.selectAll('g.series')
        .data(plotableSeries)
        .each(function(s, i){
            var type = s.dataSubtype || s.dataType,
                yAxisObj = yAxisObjs[type],
                yScale = yAxisObj.scale(),
                series = d3.select(this);
            
            series.selectAll('path.point')
                .data(d3.entries(s.calcValues))
                .each(function(d, j){
                    var xVal = d.key,
                        yVal = d.value,
                        y = yScale((type!==cc.dataSubtype.PCT) ? yVal.val : yVal.val/100),
                        x = xScale(xVal),
                        path = d3.select(this);
                    
                    if(s.aggregation === cc.aggregation.COUNT)
                        y = yScale(yVal.num);
                    
                    path.transition()
                        .duration(500)
                        .attrTween('d', pathTween(d3.svg.symbol().type('circle')(), 4))
                        //.transition()
                        //.duration(500)
                        .attr({
                            'transform': 'translate('+(x+bandWdth/2)+','+y+')'
                        })
                        .style({
                            'stroke': colors(i*2),
                            'fill': '#ffffff',
                            'stroke-width': 2
                        })
                });
            
            var line = series.selectAll('path.series-line')
                .data([1])
                .enter()
                .append('path')
                .on('mouseover',function(d){
                    plotArea.selectAll('.series')
                            .filter(function(){
                                return this !== series.node();
                            })
                            .classed('blur',true);
                    chart.svg.select('.chart-area .y-axis.'+type)
                        .style({
                            'stroke': 'RED'
                        });
                })
                .on('mouseout',function(d){
                    plotArea.selectAll('.series')
                            .filter(function(){
                                return this !== series.node();
                            })
                            .classed('blur',false);
                    chart.svg.select('.chart-area .y-axis.'+type)
                        .style({
                            'stroke': 'none'
                        });
                });
            
            line.attr({
                    'class': 'series-line',
                    'd': function(){
                        var path = d3.select(this),
                        line = seriesLine(s, chart.metaData);

                        return line(d3.entries(s.calcValues).sort(function(a,b){
                            return (a.key > b.key)?1:-1;
                        }));
                    }
                })
                .style({
                    fill: 'none',
                    'stroke-width': 2,
                    'stroke': colors(i*2)
                });
            
            var totalLength = line.node().getTotalLength();
            
            utils.moveToBack(line.node());
            
            line.attr("stroke-dasharray", totalLength + " " + totalLength)
                  .attr("stroke-dashoffset", totalLength)
                  .transition()
                    .delay(500)
                    .duration(200)
                    .ease("linear")
                    .attr("stroke-dashoffset", 0);
        });

    function seriesLine(s, meta){
        var type = s.dataSubtype || s.dataType,
            yAxisObj = meta.yAxisObjs[type],
            yScale = yAxisObj.scale();
        
        var line = d3.svg.line()
                    .interpolate("cardinal")
                    .x(function(d,i){
                        var xVal = d.key,
                            x = meta.xAxisObjs[cc.chartType.BAR].scale()(xVal);
                        
                        return x + (bandWdth/2);
                    })
                    .y(function(d,i){
                        var xVal = d.key,
                            yVal = d.value,
                            y = yScale((type!==cc.dataSubtype.PCT) ? yVal.val : yVal.val/100);
                        
                        if(s.aggregation === cc.aggregation.COUNT)
                            y = yScale(yVal.num);
                        
                        return y;
                    });
        
        return line;
    }
    
    meta.currType = cc.chartType.LINE;
}

function lineToBar(chart){
    var meta = chart.metaData,
        yAxisObjs = meta.yAxisObjs,
        xScale = meta.xAxisObjs[chart.opts.type].scale(),
        plotableSeries = meta.plotableSeries,
        bandWdth = 0 || xScale.rangeBand(),
        barWdth = bandWdth/plotableSeries.length,
        colors = d3.scale.category20();
    
    var plotArea = chart.svg.select('.plot-area');
    
    plotArea.selectAll('g.series')
        .data(plotableSeries)
        .each(function(s, i){
            var series = d3.select(this),
                type = s.dataSubtype || s.dataType;
            series.select('path.series-line').remove();
            
            series.selectAll('path.point')
                .data(d3.entries(s.calcValues))
                .each(function(d,j){
                    var xVal = d.key,
                        yVal = d.value,
                        yScale = yAxisObjs[type].scale(),
                        x = xScale(xVal),
                        y = yScale((type!==cc.dataSubtype.PCT) ? yVal.val : yVal.val/100);
                    
                    if(s.aggregation === cc.aggregation.COUNT)
                        y = yScale(yVal.num);
                    
                    var point = d3.select(this);
                    point.transition()
                        .duration(500)
                        .attr({
                            'transform': 'translate('+(x+barWdth*i)+','+y+')'
                        })
                        .style({
                            'fill': colors(i*2),
                            'stroke': '#777777',
                            'stroke-width': 1
                        })
                        .attrTween('d', pathTween(getBarPath(barWdth-2, (chart.dimensions.plotArea.height-y)), 4));
                });
        });
    
    meta.currType = cc.chartType.BAR;
}

function getBarPath(width, height){
    var x = 0,
        y = 0,
        x1 = x + width,
        y1 = y + height;

    return "M "+x+" "+y+" L "+x1+" "+y+" L "+x1+" "+y1+" L "+x+" "+y1+" z";
}

function pathTween(d1, precision) {
  return function() {
    var path0 = this,
        path1 = path0.cloneNode(),
        n0 = path0.getTotalLength(),
        n1 = (path1.setAttribute("d", d1), path1).getTotalLength();

    // Uniform sampling of distance based on specified precision.
    var distances = [0], i = 0, dt = precision / Math.max(n0, n1);
    while ((i += dt) < 1) distances.push(i);
    distances.push(1);

    // Compute point-interpolators at each distance.
    var points = distances.map(function(t) {
      var p0 = path0.getPointAtLength(t * n0),
          p1 = path1.getPointAtLength(t * n1);
      return d3.interpolate([p0.x, p0.y], [p1.x, p1.y]);
    });

    return function(t) {
      return t < 1 ? "M" + points.map(function(p) { return p(t); }).join("L") : d1;
    };
  };
}

module.exports = {
    switchTo: function (from, to, chart){
        switchers[from][to] && switchers[from][to](chart);
    },
    getBarPath: getBarPath
};