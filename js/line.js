var cc = require('./ChartConst.js'),
    d3 = require('d3');

function Chart(opts){
    var self = this;

    this.type = cc.chartType.LINE;
}
    
module.exports = {
    create: function(opts){
        return new Chart(opts);
    }
};